<?php

namespace Database\Seeders;

use App\Models\UserRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserRole::create([
            'name'  => 'Admin',
            'slug'  => Str::slug('Admin')
        ]);

        UserRole::create([
            'name'  => 'Candidate',
            'slug'  => Str::slug('Candidate')
        ]);
    }
}
