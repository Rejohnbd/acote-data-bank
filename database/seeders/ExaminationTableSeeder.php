<?php

namespace Database\Seeders;

use App\Models\Examination;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ExaminationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Examination::create([
            'examination_name'      => 'Secondary School Certificate (SSC)',
            'examination_name_slug' => Str::slug('Secondary School Certificate (SSC)')
        ]);

        Examination::create([
            'examination_name'      => 'Higher Secondary Certificate (HSC)',
            'examination_name_slug' => Str::slug('Higher Secondary Certificate (HSC)')
        ]);

        Examination::create([
            'examination_name'      => 'O Level',
            'examination_name_slug' => Str::slug('O Level')
        ]);

        Examination::create([
            'examination_name'      => 'Degree',
            'examination_name_slug' => Str::slug('Degree')
        ]);

        Examination::create([
            'examination_name'      => 'Bachelor of Arts (BA)',
            'examination_name_slug' => Str::slug('Bachelor of Arts (BA)')
        ]);
    }
}
