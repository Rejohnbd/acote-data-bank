<?php

namespace Database\Seeders;

use App\Models\SkillType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SkillTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SkillType::create([
            'skill_type_name'   => 'Technical Skills',
            'skill_type_slug'   => Str::slug('Technical Skills')
        ]);

        SkillType::create([
            'skill_type_name'   => 'Soft Skills',
            'skill_type_slug'   => Str::slug('Soft Skills')
        ]);

        SkillType::create([
            'skill_type_name'   => 'Creative Skills',
            'skill_type_slug'   => Str::slug('Creative Skills')
        ]);

        SkillType::create([
            'skill_type_name'   => 'Analytical Skills',
            'skill_type_slug'   => Str::slug('Analytical Skills')
        ]);

        SkillType::create([
            'skill_type_name'   => 'Sales Skills',
            'skill_type_slug'   => Str::slug('Sales Skills')
        ]);
    }
}
