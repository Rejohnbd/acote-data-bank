<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_skills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('candidate_id')->comment('FK from table:candidates');
            $table->unsignedBigInteger('skill_id')->comment('FK from table:skills');
            $table->string('candiate_skill_name');
            $table->string('candiate_skill_label');
            $table->timestamps();

            $table->foreign('candidate_id')
                ->references('id')
                ->on('candidates');

            $table->foreign('skill_id')
                ->references('id')
                ->on('skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_skills');
    }
};
