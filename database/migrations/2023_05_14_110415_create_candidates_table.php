<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->comment('FK from table:user');
            $table->string('data_collection_month',);
            $table->string('data_collection_year');
            $table->string('information_source');
            $table->string('name');
            $table->tinyInteger('gender');
            $table->date('date_of_birth');
            $table->string('primary_phone_number')->unique();
            $table->string('email_address')->unique();
            $table->string('city_state');
            $table->unsignedBigInteger('country_id')->comment('FK from table:countries');
            $table->string('present_address')->nullable();
            $table->unsignedBigInteger('expertise_id')->comment('FK from table:expertises');
            $table->string('lavel');
            $table->string('current_employment_status');
            $table->string('interest_work_place_type');
            $table->string('interest_job_type');
            $table->unsignedBigInteger('created_by')->comment('FK from table:user');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('created_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
};
