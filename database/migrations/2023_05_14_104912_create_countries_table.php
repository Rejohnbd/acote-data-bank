<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->char('country_code', 2)->default('');
            $table->string('country_name', 100)->default('');
            $table->char('currency_code', 3)->nullable();
            $table->char('fips_code', 2)->nullable();
            $table->char('iso_numeric', 4)->nullable();
            $table->string('north', 30)->nullable();
            $table->string('south', 30)->nullable();
            $table->string('east', 30)->nullable();
            $table->string('west', 30)->nullable();
            $table->string('capital', 30)->nullable();
            $table->string('continent_name', 100)->nullable();
            $table->char('continent', 2)->nullable();
            $table->string('languages', 100)->nullable();
            $table->char('iso_alpha3', 3)->nullable();
            $table->string('geoname_id', 20)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
};
